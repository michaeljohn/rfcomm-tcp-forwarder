/*
 * Author: Michael John
 * License: See COPYING file (public domain)
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>


/*
 * Create an RFCOMM socket and connect to the given Bluetooth address.
 *
 * Returns:
 * 	 0 Success
 * 	-1 Error
 */
int conn_rfcomm_socket(char *btaddr, int *sock)
{
	struct sockaddr_rc addr = { 0 };

	/* create a socket */
	*sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	
	/* set the connection parameters (who to connect to) */
	addr.rc_family = AF_BLUETOOTH;
	addr.rc_channel = (uint8_t) 1;
	str2ba(btaddr, &addr.rc_bdaddr);
	
	/* connect to Bluetooth endpoint */
	return connect(*sock, (struct sockaddr *)&addr, sizeof(addr));
}

/*
 * Create a TCP socket and connect to the given TCP port.
 *
 * Returns:
 * 	 0 Success
 * 	-1 Error
 */
int conn_tcp_socket(const char *ipaddr, const uint16_t port, int *sock)
{
	struct sockaddr_in addr = { 0 };
	struct in_addr dst = { 0 };
	
	/* create a socket */
	*sock = socket(AF_INET, SOCK_STREAM, 0);

	/* convert IPv4 numbers-and-dot notation into binary form */
	if (1 != inet_pton(AF_INET, ipaddr, &dst)) {
		return -1; /* invalid network address */
	}

	/* set the connection parameters */
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = dst.s_addr;
	addr.sin_port = htons(port);

	/* connect to TCP endpoint */
	return connect(*sock, (struct sockaddr *)&addr, sizeof(addr));
}

/*
 * Determine if data is available on the given socket.
 *
 * Returns:
 * 	 0 Data available
 * 	 1 Timeout while waiting for data
 * 	-1 Error, could not determine
 */
int recvtt(int sock)
{
	fd_set fds;
	struct timeval tv;
	int n;

	FD_ZERO(&fds);
	FD_SET(sock, &fds);
	tv.tv_sec = 0;
	tv.tv_usec = 100;
	n = select(sock+1, &fds, NULL, NULL, &tv);
	if (n == 0) {
		return 1; //timeout
	}
	if (n == -1) {
		return -1; //error
	}
	return 0; //data ready
}

void usage(char *app)
{
	printf("\nUSAGE: %s [BT-ADDR] [TCP-PORT] [TCP-IP]\n\n", app);
	printf("\tBT-ADDR:   Bluetooth Address of BHM\n");
	printf("\tTCP-PORT:  TCP Port to the application\n");
	printf("\tTCP-IP:    TCP IP Address to the device\n");
}

/* MAX bytes per read/write call */
#define DATA_SIZE 4096
/* MAX bytes in IP address */
#define IPADDR_SIZE 16

int main(int argc, char **argv)
{
	char *ipaddr[IPADDR_SIZE] = { 0 };
	uint16_t port;	/* TCP port */
	char data[DATA_SIZE] = { 0 };
	int sock_rc;	/* RFCOMM socket */
	int sock_tcp;	/* TCP socket */
	int status;	/* socket status */
	int n;		/* data received */

	/* sanity check on parmeters */
	if (4 != argc) {
		usage(argv[0]);
		return EINVAL;
	}

	/* convert port to number */
	port = (uint16_t)strtoul(argv[2], NULL, 10);

	ipaddr = strndup(argv[3], IPADDR_SIZE);
	
	/* open RFCOMM connection */
	if (-1 == conn_rfcomm_socket(argv[1], &sock_rc)) {
		fprintf(stderr, "ERROR: failed to connect to RFCOMM socket: %d\n", errno);
		return errno;
	}

	/* open TCP connection */
	if (-1 == conn_tcp_socket(port, &sock_tcp)) {
		fprintf(stderr, "ERROR: failed to connect to TCP port: %d\n", errno);
		return errno;
	}

	/* enter infinite loop */
	while (1) {
		memset(data, 0, DATA_SIZE);

		/* see if there is any data to read from TCP socket */
		status = recvtt(sock_tcp);
		if (0 == status) {
			/* read data */
			n = read(sock_tcp, data, DATA_SIZE);
			if (n < 0) {
				/* error reading data off socket */
				fprintf(stderr, "ERROR: failed to read data from TCP connection: %s\n", strerror(errno));
			} else if (n > 0) {
				/* forward data on RFCOMM socket */
				printf("tcp->rc: %s\n", data);
				write(sock_rc, data, n);
			}
		} else if (-1 == status) {
			/* error determining if data is available */
			fprintf(stderr, "ERROR: failed to determine data availablity on TCP connection: %s\n", strerror(errno));
		}
		/* for timeout, we don't care */

		memset(data, 0, DATA_SIZE);

		/* see if there is any data to read from the RFCOMM socket */
		status = recvtt(sock_rc);
		if (0 == status) {
			/* read data */
			n = read(sock_rc, data, DATA_SIZE);
			if (n < 0) {
				/* error reading data off socket */
				fprintf(stderr, "ERROR: failed to read data from RFCOMM connection: %s\n", strerror(errno));
			} else if (n > 0) {
				/* forward data on RFCOMM socket */
				printf("rc->tcp: %s\n", data);
				write(sock_tcp, data, n);
			}
		} else if (-1 == status) {
			/* error determining if data is available */
			fprintf(stderr, "ERROR: failed to determine data availablity on RFCOMM connection: %s\n", strerror(errno));
		}
		/* for timeout, we don't care */

	}

	/* we'll never get to here... */
	return 0;
}
